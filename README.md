# Quick Sort Algorithm
1. The partition function takes the pivot value as the last element in the array.
2. It then iterates through the array, comparing each value to the pivot value.
3. If the value is less than the pivot value, the star is incremented and it then swaps the value with the value at the star index
4. Once the loop is complete, the star index is incremented and the pivot value is swapped with the value at the star index.
5. The partition function returns the star index.
6. The quicksort function takes the star index as the pivot and recursively calls itself on the left and right sides of the pivot.

# Merge Sort Algorithm
1. The merge function takes the left and right subarrays and the middle element of the main array.
2. It checks if the middle element is smaller than the first element of the right subarray. If so, then it is already sorted.
3. If not, then it checks if the first element of the right subarray is smaller than the middle element. If so, then it moves the middle element to the left subarray.
4. It then moves the first element of the right subarray to the left subarray.
5. It then moves the middle element to the right subarray.
6. It then recursively calls itself on the left and right subarrays.
7. The merge_sort function takes the left and right indices of the main array.
8. It then calls the merge function on the left and right subarrays.
9. The merge function then sorts the subarrays and merges them.
10. The merge_sort function then recursively calls itself on the left and right subarrays.

# Selection Sort Algorithm
The algorithm divides the input list into two parts: a sorted sublist of items which is built up from left to right at the front (left) of the list and a sublist of the remaining unsorted items that occupy the rest of the list. Initially, the sorted sublist is empty and the unsorted sublist is the entire input list.
The algorithm proceeds by finding the smallest (or largest, depending on sorting order) element in the unsorted sublist, exchanging (swapping) it with the leftmost unsorted element (putting it in sorted order), and moving
the sublist boundaries one element to the right.

Finds the minimum value in the list and swaps it with the value in the first position, then find the minimum value of the remaining list and swap it with the value in the second position, and so on.

# Bubble Sort Algorithm
1. The outer for loop is responsible for the number of passes through the list.
2. The inner for loop is responsible for the bubbling process.
3. The inner for loop starts from the beginning of the list and compares every two adjacent elements.
4. If the first element is greater than the second element, the two elements are swapped.
5. The inner for loop continues to the next pair of elements, and so on.
6. Once the inner for loop completes the number of passes, the outer for loop increments the number of passes.
7. The outer for loop repeats the process until the outer for loop completes the number of passes equal to the length of the list.




# Quick Sort Algorithm
1. The partition function takes the pivot value as the last element in the array.
2. It then iterates through the array, comparing each value to the pivot value.
3. If the value is less than the pivot value, the star is incremented and it then swaps the value with the value at the star index
4. Once the loop is complete, the star index is incremented and the pivot value is swapped with the value at the star index.
5. The partition function returns the star index.
6. The quicksort function takes the star index as the pivot and recursively calls itself on the left and right sides of the pivot.

The quicksort algorithm uses both a helper function and recursion. It has an average time complexity of O(n log n) and a worst case of O(n^2). The quicksort function first checks the base case to see if the list is sorted and then calls the partition function (the helper function). The partition function sets the last value in the list as the pivot and the star as the first index - 1 (before the first element in the list). It then uses a for loop to look at each element in the list and check if it is less than or equal too the pivot value. If it is, the star is incremented and the the star value is swapped with the value for the index that the for loop was on. Once the for loop is complete, the star index is incremented and the pivot value is swapped with the value at the star index. The star index is returned and the quicksort function uses that as the pivot and recursively calls itself on the left and right sides of the pivot.


### Description of O(n log n)

This is any algorithm that repeatedly divides a set of data in half and then processes those halves independently with a helper algorithm that has a time complexity of O(N), and it will have an overall time complexity of O(N log N). Examples of this include Merge sort and Quick sort(average case).

### Description of O(n^2)

This is any algorithm that uses nested for loops. For every nested loop you are adding 1 to the power of n iterations. Examples of this are Bubble Sort, Selection sort and Quicksort(worst case)
