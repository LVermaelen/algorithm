def bubble_sort(values):
    length = len(values)
    for i in range(length - 1):
        for j in range(0, length - i - 1):
            if values[j] > values[j + 1]:
                values[j], values[j + 1] = values[j + 1], values[j]
    return values

values = [99, 23, 12, 5, 87, 65, 2]
print(bubble_sort(values))
