function selectionSort(values) {
    for (let i=0; i < values.length; i++) {
        let min_value_idx = i
        for (let j=i+1; j < values.length; j++) {
            if (values[min_value_idx] > values[j]) {
                min_value_idx = j

            }
        }
        [values[i], values[min_value_idx]] = [values[min_value_idx], values[i]]
    }
    return values
}

array = [12, 4, 6, 3, 15, 8]
console.log(selectionSort(array))
