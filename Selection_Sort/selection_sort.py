def selection_sort(values):
    for i in range(len(values)):
        min_value_index = i
        for j in range(i + 1, len(values)):
            if values[min_value_index] > values[j]:
                min_value_index = j
        values[i], values[min_value_index] = values[min_value_index], values[i]
    return values

values = [12, 4, 6, 3, 15, 8]
print(selection_sort(values))
