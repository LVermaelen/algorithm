function partition(array, left, right) {
    const pivot = array[right]
    let star = left - 1
    for (let i=left; i < right; i++) {
        if (array[i] < pivot) {
            star++
            // let temp = array[i]
            // array[i] = array[star]
            // array[star] = temp
            [array[star], array[i]] = [array[i], array[star]];
        }
    }
    star++
    // let tmp = array[right]
    // array[right] = array[star]
    // array[star] = tmp
    [array[star], array[right]] = [array[right], array[star]];
    return star
}


function quickSort(array, left=null, right=null) {
    if (left === null && right === null) {
        left = 0
        right = array.length - 1
    }
    if (left >= right) {
        return
    }
    let p = partition(array, left, right)
    quickSort(array, left, p - 1)
    quickSort(array, p + 1, right)
    return array
}
array = [12, 4, 6, 3, 15, 8]
console.log(quickSort(array, 0, array.length - 1))
