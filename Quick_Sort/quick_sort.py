def partition(values, left, right):
    pivot = values[right]
    star = left - 1
    for i in range(left, right):
        if values[i] <= pivot:
            star += 1
            values[star], values[i] = values[i], values[star]
    star += 1
    values[star], values[right] = values[right], values[star]
    return star


def quicksort(values, left, right):
    if left is None and right is None:
        left = 0
        right = len(values) - 1
    if left >= right or left < 0:
        return
    p = partition(values, left, right)
    quicksort(values, left, p - 1)
    quicksort(values, p + 1, right)
    return values

values = [12, 4, 6, 3, 15, 8]
print(quicksort(values, 0, len(values) - 1))
