function merge(values, left, middle, right) {
    if (values[middle] <= values[middle + 1]) {
        return;
    }
    let rightStart = middle + 1;
    while (left <= middle && rightStart <= right) {
        if (values[left] <= values[rightStart]) {
            left++;
        } else {
            let value = values[rightStart];
            let index = rightStart;
            while (index != left) {
                values[index] = values[index - 1];
                index--;
            }
            values[left] = value;
            left++;
            middle++;
            rightStart++;
        }
    }
}

function mergeSort(values, left, right) {
    if (left >= right) {
        return;
    }
    var middle = (left + right) >> 1;
    mergeSort(values, left, middle);
    mergeSort(values, middle + 1, right);
    merge(values, left, middle, right);
    return values
}

values = [99, 23, 12, 5, 87, 65, 2]
console.log(mergeSort(values, 0, values.length - 1))
