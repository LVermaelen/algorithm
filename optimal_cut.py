extra_memory = {}


def optimal_cut_stock(rod_length, values):
    if rod_length in extra_memory:
        return extra_memory[rod_length]
    optimal_value = values.get(rod_length, 1250)

    for cut_length in values.keys():
        cut_value = values.get(cut_length, 0)
        remaining_length = rod_length - cut_length
        if remaining_length < 0:
            continue

        optimal_remaining_value = optimal_cut_stock(remaining_length, values)

        if cut_value + optimal_remaining_value < optimal_value:
            optimal_value = cut_value + optimal_remaining_value

    extra_memory[rod_length] = optimal_value
    return optimal_value


if __name__ == "__main__":
    rod_length = 100
    rod_values = {4: 50, 1: 10, 2: 25, 3: 27, 5: 38}
    best_value = optimal_cut_stock(rod_length, rod_values)
    print("an optimal value of", best_value)
